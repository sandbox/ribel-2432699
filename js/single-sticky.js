/**
 * @file
 * Javascript for the single sticky module.
 *
 * When an administrator marks a blog post as sticky, a confirmation is
 * displayed, showing the title of the current sticky post (if any) and asking
 * the administrator to confirm the action.
 */

(function($) {
  Drupal.behaviors.singleSticky = {
    attach: function(context, settings) {
      var stickyTitle = settings.singleSticky.stickyTitle;
      $(context).delegate('#edit-sticky:checked', 'click', function() {
        $(this).attr('checked', false);
        if (confirm(Drupal.t('Current sticky post is: "@title". Are you sure you want to change it?',
            {'@title': stickyTitle}))) {
          $(this).attr('checked', true);
        }
      });
    }
  }
})(jQuery);
