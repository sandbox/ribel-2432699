<?php

/**
 * @file
 * Admin page for the Single Sticky module.
 */

/**
 * Settings form for Single Sticky.
 */
function single_sticky_admin_settings_form($form) {

  $form['single_sticky_content_types'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Content Types'),
    '#description' => t('Select content types for which you allows to have only one sticky node at the same time.'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('single_sticky_content_types', array()),
  );

  return system_settings_form($form);
}
