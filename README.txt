INTRODUCTION
------------
This module allows to have only one sticky node of given content type at the
same time.

When an administrator marks a blog post as sticky, a confirmation is displayed,
showing the title of the current sticky post (if any) and asking the
administrator to confirm the action.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/ribel/2432699

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2432699
